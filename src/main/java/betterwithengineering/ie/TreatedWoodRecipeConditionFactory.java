package betterwithengineering.ie;

import betterwithengineering.BWE;
import com.google.gson.JsonObject;
import net.minecraftforge.common.crafting.IConditionFactory;
import net.minecraftforge.common.crafting.JsonContext;

import java.util.function.BooleanSupplier;

public class TreatedWoodRecipeConditionFactory implements IConditionFactory {
    @Override
    public BooleanSupplier parse(JsonContext context, JsonObject json) {
        return () -> BWE.ConfigManager.ImmersiveEngineering.addTreatedWood;
    }
}
